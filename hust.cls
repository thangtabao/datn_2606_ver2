\LoadClass[a4paper,12pt, oneside]{book} 
\usepackage[utf8]{vietnam}
\usepackage{acronym}
\usepackage{fancybox,fancyhdr,lscape,array,setspace} % for boxes package for header and cover
\usepackage{amsthm,amssymb,amsfonts,amsmath} % for math
\usepackage{lipsum} % for dummy text
\usepackage{url}
\usepackage{multicol,multirow} % for mission ticket 
\usepackage{indentfirst} % for indentation like normal ベトナム人
\usepackage{graphicx} % for graphics
\usepackage[export]{adjustbox}
\usepackage{textcomp}
\usepackage[ruled,linesnumbered,resetcount]{algorithm2e}
\usepackage{algpseudocode}
\usepackage{booktabs}
\usepackage[table]{xcolor}
\usepackage{pdflscape}
\usepackage{longtable}
\usepackage{multirow}
\usepackage{geometry}
\usepackage{threeparttablex}
\usepackage{graphicx}
\usepackage[compatibility=false]{caption}
\usepackage{subcaption}
%\usepackage[caption=false]{subfig}
\usepackage[numbers]{natbib}
\usepackage{url}
\usepackage{rotating}

\usepackage{hyperref}
\usepackage{lipsum}
\usepackage{import}
\usepackage[acronym]{glossaries}
\makeglossaries
\newcommand{\scalefigure}{0.20}
\renewcommand\thesubfigure{\alph{subfigure}}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Bổ đề}
\theoremstyle{definition}
\newtheorem{definition}{Định nghĩa}[section]
\renewcommand{\algorithmcfname}{Thuật toán}
% footer
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\rfoot{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}

\hypersetup{
	unicode=true, 
	colorlinks=true,
	linkcolor=black,
	urlcolor=black,
	citecolor=black,
}


