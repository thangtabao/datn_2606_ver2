\contentsline {chapter}{Đề tài tốt nghiệp}{2}{Doc-Start}%
\contentsline {chapter}{Thuật ngữ}{5}{section*.6}%
\contentsline {chapter}{Lời nói đầu}{10}{chapter*.10}%
\contentsline {chapter}{\numberline {1}Cơ sở lý thuyết}{12}{chapter.1}%
\contentsline {section}{\numberline {1.1}Tổng quan về các thuật toán tối ưu}{12}{section.1.1}%
\contentsline {section}{\numberline {1.2}Giải thuật di truyền}{14}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Khởi tạo quần thể}{15}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Tính toán độ thích nghi}{15}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Chiến lược chọn lọc bố mẹ}{16}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Toán tử lai ghép}{16}{subsection.1.2.4}%
\contentsline {subsection}{\numberline {1.2.5}Toán tử đột biến}{16}{subsection.1.2.5}%
\contentsline {subsection}{\numberline {1.2.6}Chiến lược chọn lọc sinh tồn}{17}{subsection.1.2.6}%
\contentsline {subsection}{\numberline {1.2.7}Điều kiện dừng của thuật toán}{18}{subsection.1.2.7}%
\contentsline {section}{\numberline {1.3}Giải thuật tiến hóa đa nhân tố}{18}{section.1.3}%
\contentsline {chapter}{\numberline {2}Bài toán Cây khung phân cụm có đường đi ngắn nhất}{22}{chapter.2}%
\contentsline {section}{\numberline {2.1}Giới thiệu bài toán Cây khung phân cụm có đường đi ngắn nhất}{22}{section.2.1}%
\contentsline {section}{\numberline {2.2}Phát biểu bài toán}{23}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Một số định nghĩa và ký hiệu}{23}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Bài toán Cây khung phân cụm có đường đi ngắn nhất}{25}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Các nghiên cứu liên quan}{26}{section.2.3}%
\contentsline {chapter}{\numberline {3}Chiến lược mã hóa giảm chiều không gian tìm kiếm}{28}{chapter.3}%
\contentsline {section}{\numberline {3.1}Lý do xuất phát của ý tưởng}{28}{section.3.1}%
\contentsline {section}{\numberline {3.2}Biểu diễn mã hóa đề xuất}{29}{section.3.2}%
\contentsline {section}{\numberline {3.3}Phương pháp hiệu chỉnh mã hóa không hợp lệ}{30}{section.3.3}%
\contentsline {section}{\numberline {3.4}Phương pháp giải mã đề xuất}{32}{section.3.4}%
\contentsline {chapter}{\numberline {4}Giải thuật tiến hóa đa nhân tố giải bài toán Cây khung phân cụm có đường đi ngắn nhất}{37}{chapter.4}%
\contentsline {section}{\numberline {4.1}Phương pháp xây dựng không gian tìm kiếm chung}{37}{section.4.1}%
\contentsline {section}{\numberline {4.2}Khởi tạo cá thể}{39}{section.4.2}%
\contentsline {section}{\numberline {4.3}Toán tử lai ghép}{40}{section.4.3}%
\contentsline {section}{\numberline {4.4}Toán tử đột biến}{40}{section.4.4}%
\contentsline {section}{\numberline {4.5}Phương pháp xây dựng biểu diễn cá thể riêng trong từng tác vụ}{42}{section.4.5}%
\contentsline {section}{\numberline {4.6}Phương pháp đánh giá đề xuất}{44}{section.4.6}%
\contentsline {section}{\numberline {4.7}Độ phức tạp của thuật toán}{44}{section.4.7}%
\contentsline {chapter}{\numberline {5}Kết quả thực nghiệm}{46}{chapter.5}%
\contentsline {section}{\numberline {5.1}Dữ liệu thực nghiệm}{46}{section.5.1}%
\contentsline {section}{\numberline {5.2}Kịch bản thực nghiệm}{46}{section.5.2}%
\contentsline {section}{\numberline {5.3}Tiêu chí thực nghiệm}{47}{section.5.3}%
\contentsline {section}{\numberline {5.4}Phân tích dựa trên các test thống kê}{47}{section.5.4}%
\contentsline {section}{\numberline {5.5}So sánh chi tiết thuật toán K-MFEA với G-MFEA và HB-RGA trên chất lượng lời giải}{48}{section.5.5}%
\contentsline {section}{\numberline {5.6}So sánh xu hướng hội tụ của thuật toán}{50}{section.5.6}%
\contentsline {section}{\numberline {5.7}Phân tích yếu tố ảnh hưởng đến chất lượng lời giải của thuật toán}{52}{section.5.7}%
\contentsline {chapter}{\numberline {6}Kết luận}{54}{chapter.6}%
\contentsline {section}{\numberline {6.1}Các đóng góp chính}{54}{section.6.1}%
\contentsline {section}{\numberline {6.2}Các hạn chế}{54}{section.6.2}%
\contentsline {section}{\numberline {6.3}Hướng phát triển}{55}{section.6.3}%
\contentsline {chapter}{Tài liệu tham khảo}{55}{section.6.3}%
\contentsline {chapter}{Phụ lục}{58}{chapter*.41}%
